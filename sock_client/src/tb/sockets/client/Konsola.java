package tb.sockets.client;

import java.io.*;
import java.net.*;

import tb.sockets.client.MainFrame;

public class Konsola 
{
	public static int lokalizacja = 10, akceptacja=10;
	public int popLokalizacja = 10, popAkc = 10;
	static boolean tura = true, cont = true;
	static Socket sock;
	BufferedReader keyRead;
	OutputStream os;
	PrintWriter pwrite;
	InputStream is;
	BufferedReader odczyt;
	public Konsola() throws Exception
	{
		int ruchy=0;
		sock = new Socket("localhost", 6666);
		keyRead = new BufferedReader(new InputStreamReader(System.in));
		os = sock.getOutputStream(); 
		pwrite = new PrintWriter(os, true);
		is = sock.getInputStream();
		odczyt = new BufferedReader(new InputStreamReader(is));
		
		while(sock.isConnected())
		{
				while(cont==true)
				{
					pwrite.flush();
					if(tura == true)							
					{
						if(lokalizacja!=popLokalizacja)						
						{
							pwrite.write(lokalizacja);
							ruchy++;
							popLokalizacja = lokalizacja;
							tura = false;
							pwrite.flush();
							//sprawdzanie wyniku
							if(ruchy>0)							
								MainFrame.SprawdzanieWygranej();
						}
					}
					if(tura == false)							
					{	
						
						akceptacja = is.read();
						if(akceptacja != popAkc)					
						{
							MainFrame.RuchPrzeciwnika(akceptacja);	
							ruchy++;
							popAkc = akceptacja;
							tura = true;
							MainFrame.turn.setText("Twoj ruch");
							//sprawdzanie wyniku
							if(ruchy>0)							
								MainFrame.SprawdzanieWygranej();
						}
					}
					if(sock.isClosed())
					{
						cont=false;
					}
				}
				sock.close();
				os.close();
				pwrite.close();
				is.close();
				odczyt.close();
			}
	}	
}