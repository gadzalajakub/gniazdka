package tb.sockets.server;

import java.awt.Color;
import java.awt.EventQueue;
//import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.text.ParseException;

import javax.swing.JButton;
//import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
//import javax.swing.text.MaskFormatter;

import tb.sockets.server.Konsola_S;
import tb.sockets.server.OrderPane;
import tb.sockets.server.kontrolki.KKButton;

@SuppressWarnings("serial")
public class MainFrame extends JFrame 
{
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		new Thread()
		{
			public void run()
			{
				try
				{
					Konsola_S server = new Konsola_S();
					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}.start();
		
	}
	
	
	public static int [][]miejsca = new int [3][3];
	public static JButton btn1_1;
	public static JButton btn1_2;
	public static JButton btn1_3;
	public static JButton btn2_1;
	public static JButton btn2_2;
	public static JButton btn2_3;
	public static JButton btn3_1;
	public static JButton btn3_2;
	public static JButton btn3_3;
	public static JLabel turn, lblNotConnected, lblStatus, lblCzymgrasz ;
	public static OrderPane panel;
	/**
	 * Create the frame.
	 */
	public MainFrame() throws Exception
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(585,100, 485, 380);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));          //ca�a ramka 
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Serwer");
		this.setResizable(false);
		
		turn = new JLabel("");
		turn.setBounds(10, 14, 90, 14);
		contentPane.add(turn);
		
		lblStatus = new JLabel("");
		lblStatus.setBounds(10, 42, 120, 14);
		contentPane.add(lblStatus);
		
		lblCzymgrasz = new JLabel("GRASZ KRZY�YKIEM");
		lblCzymgrasz.setBounds(10, 72, 120, 14);
		contentPane.add(lblCzymgrasz);
		
	
		
	//	JButton btnConnect = new JButton("Connect");
	//	btnConnect.setBounds(10, 70, 100, 23);
	//	contentPane.add(btnConnect);
		
	//	JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
	//	frmtdtxtfldXxxx.setText("xxxx");
	//	frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
	//	contentPane.add(frmtdtxtfldXxxx);
		
	
		
		panel = new OrderPane();              //np w niej b�dziemy rysowa� co� 
		panel.setBounds(145, 14, 320, 320);
		
		contentPane.add(panel);
		
	//	lblNotConnected = new JLabel("Not Connected", SwingConstants.CENTER);
	//	lblNotConnected.setForeground(new Color(255, 255, 255));
	//	lblNotConnected.setBackground(new Color(128, 128, 128));
	//	lblNotConnected.setOpaque(true);
	//	lblNotConnected.setBounds(10, 104, 123, 23);
	//	contentPane.add(lblNotConnected);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBounds(10, 138, 123, 123);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(3, 3, 5, 5));          //(ilo�� wierszy,kolumn, odleg�osci mi�dzy nimi)
		
		btn1_1 = new KKButton();
		btn1_1.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(Konsola_S.tura == true)
					{
					miejsca[0][0]=10;
					panel.rysujObiekt(1,0);
					turn.setText("Ruch przeciwnika");
					Konsola_S.lokalizacja=0;
					btn1_1.setEnabled(false);
					}
				}
				});
		panel_1.add(btn1_1);
		
		
		btn1_2 = new KKButton();
		btn1_2.addActionListener(new ActionListener()
				{
				
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(Konsola_S.tura == true)
					{
					miejsca[0][1]=10;
					panel.rysujObiekt(1, 1);
					Konsola_S.lokalizacja=1;
					btn1_2.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		panel_1.add(btn1_2);

		btn1_3 = new KKButton();
		btn1_3.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(Konsola_S.tura == true)
					{
					miejsca[0][2]=10;
					panel.rysujObiekt(1, 2);
					Konsola_S.lokalizacja=2;
					btn1_3.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		
		panel_1.add(btn1_3);

		btn2_1 = new KKButton();
		btn2_1.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(Konsola_S.tura == true)
					{
					miejsca[1][0]=10;
					panel.rysujObiekt(1,3);
					Konsola_S.lokalizacja=3;
					btn2_1.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		panel_1.add(btn2_1);
		
		btn2_2 = new KKButton();
		btn2_2.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(Konsola_S.tura == true)
					{
					miejsca[1][1]=10;
					panel.rysujObiekt(1, 4);
					Konsola_S.lokalizacja=4;
					btn2_2.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		panel_1.add(btn2_2);
		
		btn2_3 = new KKButton();
		btn2_3.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if(Konsola_S.tura == true)
					{
					miejsca[1][2]=9;
					panel.rysujObiekt(1,5);
					Konsola_S.lokalizacja=5;
					btn2_3.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		panel_1.add(btn2_3);
		
		btn3_1 = new KKButton();
		btn3_1.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e )
				{
					if(Konsola_S.tura == true)
					{
					miejsca[2][0]=10;
					panel.rysujObiekt(1,6);
					Konsola_S.lokalizacja =6;
					btn3_1.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		panel_1.add(btn3_1);
		
		btn3_2 = new KKButton();
		btn3_2.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e )
				{
					if(Konsola_S.tura == true)
					{
					miejsca[2][1]=10;
					panel.rysujObiekt(1,7);
					Konsola_S.lokalizacja =7;
					btn3_2.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		panel_1.add(btn3_2);
		
		btn3_3 = new KKButton();
		btn3_3.addActionListener(new ActionListener()
				{
				@Override
				public void actionPerformed(ActionEvent e )
				{
					if(Konsola_S.tura == true)
					{
					miejsca[2][2]=10;
					panel.rysujObiekt(1,8);
					Konsola_S.lokalizacja=8;
					btn3_3.setEnabled(false);
					turn.setText("Ruch przeciwnika");
					}
				}
				});
		panel_1.add(btn3_3);
		

	}
	
	
	public static void SprawdzanieWygranej()
	{
		
		Graphics2D obrazek1 = (Graphics2D)panel.getGraphics();
		obrazek1.setColor(Color.GREEN);
		int licznik = 0;
		//Warunek na remis ka�de miejsce w naszej tabeli musi byc r�ne od zera �eby co kolwiek by�o tam wprowadzone
		if(miejsca[0][0]!=0 && miejsca[1][0]!=0 && miejsca[2][0]!=0 && miejsca[0][1]!=0 && miejsca[1][1]!=0 && miejsca[2][1]!=0 && miejsca[0][2]!=0 && miejsca[1][2]!=0 && miejsca[2][2]!=0)
		{
			licznik=3;
		}
		//Warunek - - -(1)
		if(miejsca[0][0]==miejsca[0][1] && miejsca[0][1]==miejsca[0][2] && miejsca[0][0]!=0 && miejsca[0][1]!=0 && miejsca[0][2]!=0)
		{
			obrazek1.drawOval(5,10,300,90);
			if(miejsca[0][0]==10)
			{
				licznik=1;   //wygrana
			}
			else
			{
				licznik=2;   //przegrana
			}
		}
		//Warunek - - -(2)
		if(miejsca[1][0]==miejsca[1][1] && miejsca[1][1]==miejsca[1][2] && miejsca[1][0]!=0 && miejsca[1][1]!=0 && miejsca[1][2]!=0)
		{
			obrazek1.drawOval(5,115,300,90);
			if(miejsca[1][0]==10)
			{
				licznik=1;
			}
			else
			{
				licznik=2;
			}
		}
		//Warunek - - -(3)
		if(miejsca[2][0]==miejsca[2][1] && miejsca[2][1]==miejsca[2][2] && miejsca[2][0]!=0 && miejsca[2][1]!=0 && miejsca[2][2]!=0)
		{
			obrazek1.drawOval(5,215,300,90);
			if(miejsca[2][0]==10)
			{
				licznik=1;
			}
			else
			{
				licznik=2;
			}
		}
		//Warunek |||(1)
		if(miejsca[0][0]==miejsca[1][0] && miejsca[1][0]==miejsca[2][0] && miejsca[0][0]!=0 && miejsca[1][0]!=0 && miejsca[2][0]!=0)
		{
			obrazek1.drawOval(15,5,90,300);
			if(miejsca[0][0]==10)
			{
				licznik=1;
			}
			else
			{
				licznik=2;
			}
		}
		//Warunek |||(2)
		if(miejsca[0][1]==miejsca[1][1] && miejsca[1][1]==miejsca[2][1] && miejsca[0][1]!=0 && miejsca[1][1]!=0 && miejsca[2][1]!=0)
		{
			obrazek1.drawOval(115,5,90,300);
			if(miejsca[0][1]==10)
			{
				licznik=1;
			}
			else
			{
				licznik=2;
			}
		}
		//Warunek |||(3)
		if(miejsca[0][2]==miejsca[1][2] && miejsca[1][2]==miejsca[2][2] && miejsca[0][2]!=0 && miejsca[1][2]!=0 && miejsca[2][2]!=0)
		{
			obrazek1.drawOval(215,5,90,300);
			if(miejsca[0][2]==9)
			{
				licznik=1;
			}
			else
			{
				licznik=2;
			}
		}
		//Warunek \\\(1)
		if(miejsca[2][2]==miejsca[1][1] && miejsca[1][1]==miejsca[0][0] && miejsca[2][2]!=0 && miejsca[1][1]!=0 && miejsca[0][0]!=0)
		{
			
			if(miejsca[2][2]==10)
			{
				licznik=1;
			}
			else
			{
				licznik=2;
			}
		}
		//Warunek ///(2)
		if(miejsca[2][0]==miejsca[1][1] && miejsca[1][1]==miejsca[0][2] && miejsca[2][0]!=0 && miejsca[1][1]!=0 && miejsca[0][2]!=0)
		{
			if(miejsca[2][0]==10)
			{
				licznik=1;
			}
			else
			{
				licznik=2;
			}
		}
		
		if(licznik>0)
		{
			if(licznik==1)
			{
				//wygrana
				lblStatus.setText("WYGRALES");
			}
			else if(licznik==2)
			{
				//przegrana
				lblStatus.setText("PRZEGRALES");
			}
			else
			{
				//remis
				lblStatus.setText("REMIS");
			}
			
			
		}
	
	}
	public static void RuchPrzeciwnika(int x)
 	{
 		switch(x/3)
 		{
 		case 0:
 		{
 			switch(x%3)
 			{
 			case 0: 
 					panel.rysujObiekt(0, 0); 
 					btn1_1.setEnabled(false); 
 					miejsca[0][0] = 9; 
 					break;
 			case 1: 
 					panel.rysujObiekt(0, 1); 
 					btn1_2.setEnabled(false); 
 					miejsca[0][1] = 9; 
 					break;
 			case 2: 
 					panel.rysujObiekt(0, 2); 
 					btn1_3.setEnabled(false); 
 					miejsca[0][2] = 9; 
 					break;
 			}
 			break;
 		}
 		case 1:
 		{
 			switch(x%3)
 			{
 			case 0: 
 					panel.rysujObiekt(0, 3); 
 					btn2_1.setEnabled(false); 
 					miejsca[1][0] = 9; 
 					break;
 			case 1: 
 					panel.rysujObiekt(0, 4); 
 					btn2_2.setEnabled(false); 
 					miejsca[1][1] = 9; 
 					break;
 			case 2: 
 					panel.rysujObiekt(0, 5); 
 					btn2_3.setEnabled(false); 
 					miejsca[1][2] = 9; 
 					break;
 			}
 			break;
 		}
 		case 2:
 		{
 			switch(x%3)
 			{
 			case 0: 
 					panel.rysujObiekt(0, 6); 
 					btn3_1.setEnabled(false); 
 					miejsca[2][0] = 9; 
 					break;
 			case 1: 
 					panel.rysujObiekt(0, 7); 
 					btn3_2.setEnabled(false); 
 					miejsca[2][1] = 9; break;
 			case 2: 
 					panel.rysujObiekt(0, 8); 
 					btn3_3.setEnabled(false); 
 					miejsca[2][2] = 9; 
 					break;
 			}
 			break;
 		}
 		}
 	}

}