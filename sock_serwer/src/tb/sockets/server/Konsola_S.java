package tb.sockets.server;

import java.io.*;
import java.net.*;

import tb.sockets.server.MainFrame;

public class Konsola_S 
{
	public static int lokalizacja = 10, akceptacja=10;
	public int popLokalizacja = 10, popAkc = 10;
	static boolean tura = false, cont = true;
	static ServerSocket sersock;
	Socket sock;
	//BufferedReader keyRead;
	OutputStream os;
	PrintWriter pwrite;
	InputStream is;
	BufferedReader odczyt;
	public Konsola_S() throws Exception
	{
		int ruchy =0;
		sersock = new ServerSocket(6666);
	    sock = sersock.accept();
	    os = sock.getOutputStream(); 
	    pwrite = new PrintWriter(os, true);
	    is = sock.getInputStream();
	    odczyt = new BufferedReader(new InputStreamReader(is));
		
		if(sock.isConnected())
		{
				while(cont == true)
				{
					pwrite.flush();
					if(tura == true)					
					{
						if(lokalizacja!= popLokalizacja)			
						{
							pwrite.write(lokalizacja);
							ruchy++;
							popLokalizacja = lokalizacja;
							tura = false;
							pwrite.flush();
							//sprawdzanie wyniku
							if(ruchy>0)					
								MainFrame.SprawdzanieWygranej();
						}
					}
					if(tura == false)					
					{
						
						akceptacja = odczyt.read();
						if(akceptacja != popAkc)			
						{
							MainFrame.RuchPrzeciwnika(akceptacja);	
							ruchy++;
							popAkc = akceptacja;
							tura = true;
							MainFrame.turn.setText("Twoj ruch");
							//sprawdzanie wyniku
							if(ruchy>0)					
								MainFrame.SprawdzanieWygranej();
						}
					}
					if(sock.isClosed())
					{
						cont=false;
					}
				}
			}
				sock.close();
				os.close();
				pwrite.close();
				is.close();
				odczyt.close();
	}
}